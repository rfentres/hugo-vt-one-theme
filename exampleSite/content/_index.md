+++
title = "Home"
description = "home:::test for content"
date = "2018-02-11"
menu = "mainmenu"
type = "home"
layout="home"
+++

{{< vtCallVideo vtctaTitle="Are you a transfer student interested in undergraduate research?" vtctaDescription="Listen to Brandi and Chase share their experiences both transfer and first-generation students in celebration of National Transfer Week." vtctaVideoLink="https://cdnapisec.kaltura.com/p/2375811/sp/237581100/embedIframeJs/uiconf_id/41951101/partner_id/2375811?iframeembed=true&playerId=kplayer&entry_id=1_nrevtaok&flashvars[streamerType]=auto" vtctaVideoID="vt_video_6a2a7dce-e8b7-46bf-b6bd-a54fca9c5e2c" vtctaVideoTitle="Brandi Thomas and Chase Mullins" >}}

