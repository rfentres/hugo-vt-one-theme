+++
title = "About"
description = "about-test for shortcodes and content"
date = "2018-02-11"
menu = "mainmenu"
+++
{{< imageBlock imageTitle="Highlighted image w/blockquotes" imageDesc="The Virginia Tech campus is truly unique in its use of locally-sourced limestone for its building facades." imageCiteAuthor="Kee Ho" imageCiteTitle1="Architecture Yesterday" image="https://vtnews.vt.edu/content/dam/vtnews_vt_edu/articles/2012/05/images/050712-corps-pylons.jpg" imageAlt="Virginia Tech War Memorial" imageCardTitle="War Memorial" imageCardText="One of the iconic landmarks on the Virginia Tech campus." moreImagesLink="https://goo.gl/YJzajf" >}}


{{< imageBlob imageTitle="Image with Caption" image="https://static.boredpanda.com/blog/wp-content/uploads/2016/08/cute-kittens-30-57b30ad41bc90__605.jpg" imgAlt="kittens" imageText="Sed blandit libero volutpat sed cras ornare. Penatibus et magnis dis parturient montes nascetur. Malesuada bibendum arcu vitae elementum curabitur. Risus feugiat in ante metus dictum. Eget nunc scelerisque viverra mauris in. Vitae proin sagittis nisl rhoncus mattis rhoncus urna neque. Turpis nunc eget lorem dolor sed. Pellentesque pulvinar pellentesque habitant morbi. Nisi lacus sed viverra tellus in hac. Quisque non tellus orci ac auctor augue mauris augue.</p><p>Iaculis eu non diam phasellus vestibulum. Rhoncus urna neque viverra justo nec ultrices dui sapien eget. Amet aliquam id diam maecenas ultricies mi eget. Bibendum enim facilisis gravida neque convallis a cras semper auctor. Rhoncus dolor purus non enim praesent elementum facilisis. Sit amet porttitor eget dolor morbi non arcu risus quis. Bibendum at varius vel pharetra vel turpis. Vel pretium lectus quam id leo in vitae. Quis viverra nibh cras pulvinar. Mauris nunc congue nisi vitae suscipit tellus. Faucibus in ornare quam viverra. Massa vitae tortor condimentum lacinia. Duis ut diam quam nulla. Velit dignissim sodales ut eu." imageCaption="Everybody Loves Kittens" >}}




